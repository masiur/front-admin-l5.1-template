<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Demo extends Model
{
	 use Eloquence;

	// fulltext searchable columns for indexing
	protected $searchableColumns = [
		'name', 
		'email',
		'company_email',
		'reg', 
		'address',
		'country', 
		'uuid', 
		'locale', 
		'cc_num', 
		'timezone',
		'sex'
	];

    protected $table = 'demo';
}
