<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Demo::class, function (Faker\Generator $faker) {
   	
   	$sex = $faker->randomElement(['male', 'female']);

    return [
        'name' => $faker->name($sex),
        'company_email' => $faker->unique()->companyEmail,
        'email' => $faker->unique()->email,
        'reg' => $faker->randomNumber(6),
        'sex' => $sex,
        'birthday' => $faker->date,
        'address' => $faker->address,
        'country' => $faker->country,
        'uuid' => $faker->uuid,
        'locale' => $faker->locale,
        'cc_num' => $faker->creditCardNumber,
        'timezone' => $faker->timezone,
    ];
});


