<?php

use App\Models\Demo;
use Illuminate\Database\Seeder;

class DemoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($loop=0; $loop < 5; $loop++) { 
            factory(App\Demo::class, 10000)->create();
        }
    }
}
