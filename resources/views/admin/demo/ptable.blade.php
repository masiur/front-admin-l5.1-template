@extends('layouts.default')
@section('content')

    <div class="row">
        <div class="col-lg-12">
            
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
@include('admin.includes.alert')

                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>{{ $title }}</h4>
                                </div>
                                <div class="col-md-6"> 
                                {{-- <a class="pull-right" href="{!! route('demo.create') !!}"><button class="btn btn-success">Add Demo</button></a> --}}
                                    
                                </div>
                            </div>
                        </div>            
                        <div class="panel-body"  style="overflow-y:auto">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    {!! Form::open(['route'=>'demo.index', 'method'=> 'GET']) !!}   
                                     {!! Form::text('key', Input::get('key'), ['placeholder'=>'Search Name', 'class'=>'pull-right']) !!}
                                    {!! Form::close() !!}
                                    <table  id="dataTable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Name</th>
                                            <th>Company Email</th>
                                            <th>Email</th>
                                            <th>Reg No.</th>
                                            <th>Sex</th>
                                            <th>Birthday</th>
                                            <th>Address</th>
                                            <th>Country</th>
                                            <th>UUID</th>
                                            <th>Locale</th>
                                            <th>Credit Card</th>
                                            <th>Timezone</th>
                                            <th>#</th>
                                            <th>#</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($demos as $demo)

                                            <tr>
                                                <td>{!! $demo->id !!}</td>
                                                <td>
                                                    <a class="show-demo-modal" data-toggle="modal"  data-demo-id="{{ $demo->id }}" data-demo-url="{!! route('demo.show',$demo->id) !!}"  href="#" style="">{!! $demo->name !!}</a>
                                                </td>
                                                <td>{!! $demo->company_email !!}</td>
                                                <td>{!! $demo->email !!}</td>
                                                <td>{!! $demo->reg !!}</td>
                                                <td>{!! $demo->sex !!}</td>
                                                <td>{!! $demo->birthday !!}</td>
                                                <td>{!! $demo->address !!}</td>
                                                <td>{!! $demo->country !!}</td>
                                                <td>{!! $demo->uuid !!}</td>
                                                <td>{!! $demo->locale !!}</td>
                                                <td>{!! $demo->cc_num !!}</td>
                                                <td>{!! $demo->timezone !!}</td>

                                                <td><a class="btn btn-success btn-xs btn-archive edit-demo-modal" data-toggle="modal"  data-demo-id="{{ $demo->id }}" data-demoUpdateUrl="{!! route('demo.update',$demo->id) !!}" data-demo-url="{!! route('demo.edit',$demo->id)!!}" href="#" style="margin-right: 3px;">Edit</a></td>
                                                <td><a href="#" class="btn btn-danger btn-xs btn-archive deleteBtn" data-toggle="modal" data-demo-url="{!! route('demo.delete',$demo->id) !!}" data-target="#deleteConfirm" deleteId="{!! $demo->id !!}">Delete</a></td>
                                            </tr>
                                            
                                        @endforeach
                                        </tbody>
                                    </table>
                                <div class="pull-right create-demo-modal">
                                    {!! $demos->appends(['key'=> Input::get('key')])->render() !!}
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 
@include('admin.sections.modals')  <!-- the two modals of this page  is kept in another folder for convenience  -->


@stop

@section('style')

{!! Html::style('assets/datatables/jquery.dataTables.min.css') !!} 

@endsection

@section('script')

{{-- 
{!! Html::script('assets/datatables/jquery.dataTables.min.js') !!}
{!! Html::script('assets/datatables/dataTables.bootstrap.js') !!}

--}}


    <!-- for Datatable -->
    <!-- <script type="text/javascript">

        $(document).ready(function() {
            
            $('#dataTable').dataTable();
            $(document).on("click", ".deleteBtn", function() {
                var deleteId = $(this).attr('deleteId');
                var url = "<?php echo URL::route('demo.index'); ?>";
                $(".deleteForm").attr("action", url+'/'+deleteId);
            });

        });
    </script> -->

@stop







