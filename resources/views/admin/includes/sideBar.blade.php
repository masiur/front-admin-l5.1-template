 <!-- Aside Start-->
<aside class="left-panel">

            <!-- brand -->
            <div class="logo">
                <a href="#" class="logo-expanded">
                    <i class="ion-social-buffer"></i>
                    <span href="{!!route('dashboard')!!}" class="nav-label">{!! Config::get('customConfig.names.siteName')!!}</span>

                </a>
            </div>
            <!-- / brand -->


            <!-- Navbar Start -->
            <nav class="navigation">
                <ul class="list-unstyled">

                     <li class="{!! Menu::isActiveRoute('dashboard') !!}"><a href="{{ route('dashboard') }}"><i class="ion-flask"></i> <span class="nav-label">Dashboard</span></a>                 
                    </li>


                    <li class="{!! Menu::isActiveRoute('demo.index') !!}"><a href="{{ route('demo.index') }}"><i class="ion-jet"></i> <span class="nav-label">Paginated Demo CRUD</span></a>                       
                    </li>

                    {{-- <li class="{!! Menu::isActiveRoute('demo.dtabledemo') !!}"><a href="{{ route('demo.ptable') }}"><i class="ion-jet"></i> <span class="nav-label"></span></a>                       
                    </li> --}}

                    <li class="has-submenu {!! Menu::areActiveRoutes(['category.index', 'category.create']) !!}"><a href="#"><i class="ion-email"></i> <span class="nav-label">Category</span></a>
                        <ul class="list-unstyled">
                            <li class="{!! Menu::isActiveRoute('category.index') !!}"><a href="{{ route('category.index') }}">List of Categories</a></li>
                            <li class="{!! Menu::isActiveRoute('category.create') !!}"><a href="{{ route('category.create') }}">Add a Category</a></li>
                        </ul>
                    </li>


                    <li class="has-submenu"><a href="#"><i class="ion-grid"></i> <span class="nav-label">Data Tables</span></a>
                        <ul class="list-unstyled">
                            <li><a href="#">Basic Tables</a></li>
                            <li><a href="#">Data Table</a></li>
                            
                        </ul>
                    </li>

                    

                    {{-- 
                    <li class="has-submenu"><a href="#"><i class="ion-stats-bars"></i> <span class="nav-label">Charts</span><span class="badge bg-purple">1</span></a>
                        <ul class="list-unstyled">
                            <li><a href="#">chart</a></li>
                            <li><a href="#">Morris</a></li>

                        </ul>
                    </li>

                    <li class="has-submenu"><a href="#"><i class="ion-location"></i> <span class="nav-label">Maps</span></a>
                        <ul class="list-unstyled">
                            <li><a href="gmap.html"> Google Map</a></li>
                            <li><a href="vector-map.html"> Vector Map</a></li>
                        </ul>
                    </li>
                    --}}

                </ul>
            </nav>



</aside>
        <!-- Aside Ends-->



