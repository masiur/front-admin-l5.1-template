       <!-- //////////////////////////////////
	//////////////MAIN HEADER///////////// 
	////////////////////////////////////-->
        <header class="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <a href="{{ route('index') }}" class="logo">
                            <img src="img/logo-small.png" alt="Image Alternative text" title="Image Title" />
                        </a>
                    </div>
                    <div class="col-md-6">
                        <div class="flexnav-menu-button" id="flexnav-menu-button">Menu</div>
                        <nav>
                            <ul class="nav nav-pills flexnav" id="flexnav" data-breakpoint="800">
                                <li class="active"><a href="{{ route('index') }}">Home</a>
                                    <!-- <ul>
                                        <li><a href="index-shop-layout-1.html">Shop Layout</a>
                                            <ul>
                                                <li><a href="index-shop-layout-1.html">Layout 1</a>
                                                </li>
                                                <li><a href="index-shop-layout-2.html">Layout 2</a>
                                                </li>
                                                <li><a href="index-shop-layout-3.html">Layout 3</a>
                                                </li>
                                                <li><a href="index-shop-layout-4.html">Layout 4</a>
                                                </li>
                                                <li><a href="index-shop-layout-5.html">Layout 5</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="index-coupon-layout-1.html">Coupon Layout</a>
                                            <ul>
                                                <li><a href="index-coupon-layout-1.html">Layout 1</a>
                                                </li>
                                                <li><a href="index-coupon-layout-2.html">Layout 2</a>
                                                </li>
                                                <li><a href="index-coupon-layout-3.html">Layout 3</a>
                                                </li>
                                                <li><a href="index.html">Layout 4</a>
                                                </li>
                                                <li><a href="index-coupon-layout-5.html">Layout 5</a>
                                                </li>
                                                <li><a href="index-coupon-layout-6.html">Layout 6</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="active"><a href="index-header-layout-1.html">Headers</a>
                                            <ul>
                                                <li class="active"><a href="index-header-layout-1.html">Layout 1</a>
                                                </li>
                                                <li><a href="index-header-layout-2.html">Layout 2</a>
                                                </li>
                                                <li><a href="index-header-layout-3.html">Layout 3</a>
                                                </li>
                                                <li><a href="index-header-layout-4.html">Layout 4</a>
                                                </li>
                                                <li><a href="index-header-layout-5.html">Layout 5</a>
                                                </li>
                                                <li><a href="index-header-logged-user.html">Logged User</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul> -->
                                </li>
                                <!-- <li><a href="category-page-shop.html">Category</a>
                                    <ul>
                                        <li><a href="category-page-shop.html">Shop</a>
                                        </li>
                                        <li><a href="category-page-coupon.html">Coupon</a>
                                        </li>
                                        <li><a href="category-page-thumbnails-shop-layout-1.html">Thumbnails</a>
                                            <ul>
                                                <li><a href="category-page-thumbnails-shop-layout-1.html">Shop</a>
                                                    <ul>
                                                        <li><a href="category-page-thumbnails-shop-layout-1.html">Layout 1</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-shop-layout-2.html">Layout 2</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-shop-layout-3.html">Layout 3</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-shop-layout-4.html">layout 4</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-shop-layout-5.html">Layout 5</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-shop-layout-6.html">Layout 6</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-shop-horizontal.html">Horizontal</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li><a href="category-page-thumbnails-coupon-layout-1.html">Coupon</a>
                                                    <ul>
                                                        <li><a href="category-page-thumbnails-coupon-layout-1.html">Layout 1</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-coupon-layout-2.html">Layout 2</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-coupon-layout-3.html">Layout 3</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-coupon-layout-4.html">Layout 4</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-coupon-layout-5.html">Layout 5</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-coupon-layout-6.html">Layout 6</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-coupon-layout-7.html">Layout 7</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-coupon-layout-8.html">Layout 8</a>
                                                        </li>
                                                        <li><a href="category-page-thumbnails-coupon-horizontal.html">Horizontal</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li><a href="category-page-thumbnails-breadcrumbs.html">Breadcrumbs</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li> -->
                                <li><a href="product-shop-sidebar.html">Product </a>
                                    <!-- <ul>
                                        <li><a href="product-shop-sidebar.html">Shop</a>
                                            <ul>
                                                <li><a href="product-shop-sidebar.html">Sidebar</a>
                                                </li>
                                                <li><a href="product-shop-sidebar-left.html">Sidebar Left</a>
                                                </li>
                                                <li><a href="product-shop-centered.html">Centered</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="product-coupon-default.html">Coupon</a>
                                            <ul>
                                                <li><a href="product-coupon-default.html">Default</a>
                                                </li>
                                                <li><a href="product-coupon-meta-right.html">Meta right</a>
                                                </li>
                                                <li><a href="product-coupon-gallery.html">Gallery</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul> -->
                                </li>

                                {{--

                                <li><a href="features-typography.html">Features</a>
                                    <ul>
                                        <li><a href="features-typography.html">Typography</a>
                                        </li>
                                        <li><a href="features-elements.html">Elements</a>
                                        </li>
                                        <li><a href="features-grid.html">Grid</a>
                                        </li>
                                        <li><a href="features-icons.html">Icons</a>
                                        </li>
                                        <li><a href="features-image-hover.html">Image Hovers</a>
                                        </li>
                                        <li><a href="features-sliders.html">Sliders</a>
                                        </li>
                                        <li><a href="features-media.html">Media</a>
                                        </li>
                                        <li><a href="features-lightbox.html">Lightbox</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="blog-sidebar-right.html">Blog</a>
                                    <ul>
                                        <li><a href="blog-sidebar-right.html">Sidebar Right</a>
                                        </li>
                                        <li><a href="blog-sidebar-left.html">Sidebar Left</a>
                                        </li>
                                        <li><a href="blog-full-width.html">Full Width</a>
                                        </li>
                                        <li><a href="post-sidebar-right.html">Post</a>
                                            <ul>
                                                <li><a href="post-sidebar-right.html">Sidebar Right</a>
                                                </li>
                                                <li><a href="post-sidebar-left.html">Sidebar Left</a>
                                                </li>
                                                <li><a href="post-full-width.html">Full Width</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="page-full-width.html">Pages</a>
                                    <ul>
                                        <li><a href="page-my-account-settings.html">My Account</a>
                                            <ul>
                                                <li><a href="page-my-account-settings.html">Settings</a>
                                                </li>
                                                <li><a href="page-my-account-addresses.html">Address Book</a>
                                                </li>
                                                <li><a href="page-my-account-orders.html">Orders History</a>
                                                </li>
                                                <li><a href="page-my-account-wishlist.html">Wishlist</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="page-full-width.html">Full Width</a>
                                        </li>
                                        <li><a href="page-sidebar-right.html">Sidebar Right</a>
                                        </li>
                                        <li><a href="page-sidebar-left.html">Sidebar Left</a>
                                        </li>
                                        <li><a href="page-faq.html">Faq</a>
                                        </li>
                                        <li><a href="page-about-us.html">About us</a>
                                        </li>
                                        <li><a href="page-team.html">Team</a>
                                        </li>
                                        <li><a href="page-cart.html">Shopping Cart</a>
                                        </li>
                                        <li><a href="page-checkout.html">Checkout</a>
                                        </li>
                                        <li><a href="page-404.html">404</a>
                                        </li>
                                        <li><a href="page-search.html">Search</a>
                                            <ul>
                                                <li><a href="page-search-black.html">Black</a>
                                                </li>
                                                <li><a href="page-search-white.html">White</a>
                                                </li>
                                                <li><a href="page-search-sticky.html">Sticky</a>
                                                </li>
                                                <li><a href="page-search-no-search.html">No Search</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="page-contact.html">Contact</a>
                                        </li>
                                    </ul>
                                </li>

                                --}}

                            </ul>
                        </nav>
                    </div>
                    <div class="col-md-4">
                        <ul class="login-register">
                            {{-- 
                            <li class="shopping-cart"><a href="page-cart.html"><i class="fa fa-shopping-cart"></i>My Cart</a>
                                <div class="shopping-cart-box">
                                    <ul class="shopping-cart-items">
                                        <li>
                                            <a href="product-shop-sidebar.html">
                                                <img src="img/amaze_70x70.jpg" alt="Image Alternative text" title="AMaze" />
                                                <h5>New Glass Collection</h5><span class="shopping-cart-item-price">$150</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="product-shop-sidebar.html">
                                                <img src="img/gamer_chick_70x70.jpg" alt="Image Alternative text" title="Gamer Chick" />
                                                <h5>Playstation Accessories</h5><span class="shopping-cart-item-price">$170</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <ul class="list-inline text-center">
                                        <li><a href="page-cart.html"><i class="fa fa-shopping-cart"></i> View Cart</a>
                                        </li>
                                        <li><a href="page-checkout.html"><i class="fa fa-check-square"></i> Checkout</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            --}}
                            <li><a class="popup-text" href="#login-dialog" data-effect="mfp-move-from-top"><i class="fa fa-sign-in"></i>Sign in</a>
                            </li>
                            <li><a class="popup-text" href="#register-dialog" data-effect="mfp-move-from-top"><i class="fa fa-edit"></i>Sign up</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>