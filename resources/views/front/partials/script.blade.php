        <!-- Scripts queries -->
        <script src="{{asset('frontend/js/jquery.js')}}"></script>
        <script src="{{asset('frontend/js/boostrap.min.js')}}"></script>
        <script src="{{asset('frontend/js/countdown.min.js')}}"></script>
        <script src="{{asset('frontend/js/flexnav.min.js')}}"></script>
        <script src="{{asset('frontend/js/magnific.js')}}"></script>
        <script src="{{asset('frontend/js/tweet.min.js')}}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
        <script src="{{asset('frontend/js/fitvids.min.js')}}"></script>
        <script src="{{asset('frontend/js/mail.min.js')}}"></script>
        <script src="{{asset('frontend/js/ionrangeslider.js')}}"></script>
        <script src="{{asset('frontend/js/icheck.js')}}"></script>
        <script src="{{asset('frontend/js/fotorama.js')}}"></script>
        <script src="{{asset('frontend/js/card-payment.js')}}"></script>
        <script src="{{asset('frontend/js/owl-carousel.js')}}"></script>
        <script src="{{asset('frontend/js/masonry.js')}}"></script>
        <script src="{{asset('frontend/js/nicescroll.js')}}"></script>

        <!-- Custom scripts -->
        <script src="{{asset('frontend/js/custom.js')}}"></script>
        <script src="{{asset('frontend/js/switcher.js')}}"></script>