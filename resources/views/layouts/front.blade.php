<!DOCTYPE html>
<html lang="en">
@include('front.partials.header')

<body>
<div class="global-wrap">
	@include('front.partials.navMenu')


    {{-- @include('front.partials.usermenu') --}}

	 @include('front.partials.authContents')

    @yield('content')


    {{--include rightSideBar--}}


    @include('front.partials.footer')
    @include('front.partials.script')

    @yield('script')
</div>
</body>
</html>